Android Activities:
===================

CharactersScroll = has the list of characters with infinity scroll
CharacterInfo = displays character information


Another class is created named Marvel which handles API calls to marvel's server.


Marvel API
===========

We may be able to use only one endpoint as /characters is already sufficient with character details but to follow the written instruction, I have used both /characters and /characters/{id}

To enable pagination on the queries, get parameters must be added for offset and limit. Not obvius in the documentation but done through experiment.

The authentication of the request relies on the hash which is an md5. the MD5 method inside Marvel class is a copy from a source so as I can focus more on the app's logic


Infinity Scroll
================

The infinity scroll is ran through a listener on the scrollview. when scroll reaches the last item, fetching of another 20 characters start.


Challenges
===========

Due to my time in Ionic lately, I had to review UI concepts for android. It was a slow start but I got a hold of it after a few minutes.