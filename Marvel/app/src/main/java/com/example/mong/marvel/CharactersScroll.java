package com.example.mong.marvel;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;

public class CharactersScroll extends AppCompatActivity {

    Marvel marvel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characters_scroll);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Test internet Connection
        if(this.isNetworkAvailable(this)) {
            // set infinite scroll
            NestedScrollView nscrollview = (NestedScrollView) this.findViewById(R.id.inc_charScroll);
            Log.d("isNull", nscrollview.toString());
            nscrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                        showPBar();
                        marvel.getCharacters(true);
                    }
                }
            });

            // add initial items
            marvel = new Marvel(this);
            marvel.getCharacters();
            this.showPBar();
        } else {
            Toast.makeText(getApplicationContext(), "NO INTERNET CONNECTION", Toast.LENGTH_LONG).show();
        }
    }

    public void addItems(CharactersScroll c, JSONObject[] items) {
        Log.d("OBJECT", items.toString());
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent = c.findViewById(R.id.inc_charScroll);
        LinearLayout charList = parent.findViewById(R.id.charList);

        // add items to LinearLayout
        for(final JSONObject item : items) {
            RelativeLayout child = (RelativeLayout) inflater.inflate(R.layout.item_character, null);
            child.setClickable(true);
            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        marvel.getCharacter(item.getInt("id")+"");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            TextView tv = (TextView) child.findViewById(R.id.tv_charItem);
            try {
                tv.setText(item.getString("name"));
                charList.addView(child);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        this.hidePBar();
    }

    public void hidePBar() {
        View pbar = this.findViewById(R.id.progressBar);
        pbar.setVisibility(View.GONE);
    }

    public void showPBar() {
        View pbar = this.findViewById(R.id.progressBar);
        pbar.setVisibility(View.VISIBLE);
    }

    public boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_characters_scroll, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
