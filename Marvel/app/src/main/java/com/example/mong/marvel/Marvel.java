package com.example.mong.marvel;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by mong on 6/15/17.
 */

public class Marvel {

    String baseUrl ="https://gateway.marvel.com/v1/public";
    String publicKey = "278dfabcecda46b66d1c51faa50bb505";
    String privateKey = "937b56bdb3840d70ec1b5d5e6c05036d0ea6569f";
    Context context = null;

    int offset = 0;
    int limit = 20;

    int errors = 10;

    CharactersScroll cs = null;

    public Marvel(CharactersScroll c) {
        this.context = c;
        this.cs = c;
    }

    public void getCharacters() {
        this.sendRequest("/characters", 1, "");
    }

    public void getCharacters(boolean more) {
        offset = offset+limit;
        String extra = "&offset="+offset+"&limit="+limit;
        this.sendRequest("/characters", 1, extra);
    }

    public void getCharacter(String id) {
        this.sendRequest("/characters/"+id, 2, "");
    }

    public void sendRequest(final String endpoint, final int cbID, final String extra) {
        // generate credential parameters
        Random rand = new Random();
        String ts = rand.nextInt()+"";
        String hash = this.md5(ts+this.privateKey+this.publicKey);

        String url = this.baseUrl+endpoint+"?ts="+ts+"&apikey="+this.publicKey+"&hash="+hash+extra;
        Log.i("URL", url);

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.context);

        final Marvel instance = this;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        switch (cbID) {
                            case 1:
                                instance.buildCharacterDS(response);
                                break;
                            case 2:
                                instance.buildCharacterInfoDS(response);
                                break;
                            default:
                                break;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errors = errors-1;
                if (errors <= 0)
                    return;

                sendRequest(endpoint, cbID, extra);
                Log.i("Response", "Response is: Error");
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void buildCharacterDS(String raw) {
        // Display the first 500 characters of the response string.
        try {
            JSONObject jsonResponse=new JSONObject(raw);
            JSONObject data = jsonResponse.getJSONObject("data");
            JSONArray characters = data.getJSONArray("results");
            JSONObject[] result = new JSONObject[characters.length()];
            for (int i=0 ; i<characters.length() ; i++) {
                result[i] = new JSONObject(characters.get(i).toString());
                Log.d("Character", characters.get(i).toString());
            }
            this.cs.addItems(cs, result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void buildCharacterInfoDS(String raw) {
        // Display the first 500 characters of the response string.
        try {
            JSONObject jsonResponse = new JSONObject(raw);
            JSONObject data = jsonResponse.getJSONObject("data");
            JSONObject character = new JSONObject(data.getJSONArray("results").get(0).toString());
            Log.d("Character", character.toString());

            JSONObject imageOBJ = character.getJSONObject("thumbnail");
            String imageURL = imageOBJ.getString("path").replace("\\", "") + "." + imageOBJ.getString("extension");

            String charName = character.getString("name");
            String charDesc = character.getString("description");

            Intent intent = new Intent(this.context, CharacterInfo.class);
            intent.putExtra("image", imageURL);
            intent.putExtra("name", charName);
            intent.putExtra("description", charDesc);
            this.context.startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
